// Soal No 1
console.log("Soal No 1 \n\n")

class Person {
	constructor(firstName, lastName, gender, birthYear){
		this.firstName = firstName
		this.lastName = lastName
		this.gender = gender
		this.birthYear = birthYear
	}

	getAge(){
		const year_now = new Date().getFullYear()
		if(!this.birthYear || year_now < this.birthYear) return "Invalid Birth Year"
		return year_now-this.birthYear
	}

	getFullName(){
		return `${this.firstName} ${this.lastName}`
	}

	getResult(){
		const result = {
			firstName : this.firstName,
			lastName : this.lastName,
			gender : this.gender,
			age : this.getAge()
		}
		return result
	}
}
const arrayToObject = function(arr){
	arr.forEach(function(item, index){
		person = new Person(item[0],item[1],item[2],item[3])
		console.log(index+1, person.getFullName(), " : " ,person.getResult())
	})
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 


// Soal No 2
console.log("\n\nSoal No 2 \n\n")

const salesProduct = [
	{item : 'Sepatu brand Stacattu', price: 1500000},
	{item : 'Baju brand Zoro', price: 500000},
	{item : 'Baju brand H&N', price: 250000},
	{item : 'Sweater brand Uniklooh', price: 175000},
	{item : 'Casing Handphone', price: 50000}
]

const shoppingTime = function(memberId, money) {
	if(!memberId) return 'Mohon maaf, toko X hanya berlaku untuk member saja'
	if(money < 50000) return 'Mohon maaf, uang tidak cukup'
	
	let changeMoney = money;
	let sortedSalesProduct = salesProduct.sort(function(a, b){return b.price-a.price})
	let listPurchased = []
	for(product of sortedSalesProduct){
		if(changeMoney >= product.price) {
			listPurchased.push(product.item)
			changeMoney -= product.price
		}
	}
	return {
		memberId: memberId,
		money: money,
		listPurchased: listPurchased,
		changeMoney: changeMoney
	}
}


// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


function naikAngkot(arrPenumpang) {
  	rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  	result = []
  	for(penumpang of arrPenumpang){
  		let start = penumpang[1]
  		let end = penumpang[2]
  		let startNum = 0;
  		let endNum = 0;
  		for(let i = 0; i < rute.length; i++){
  			if(rute[i] == start) startNum = i
  			if(rute[i] == end) endNum = i
  		}
  		let jarak = endNum-startNum
  		let bayar = jarak*2000
  		result.push({penumpang:penumpang[0], naikDari:start, tujuan:end, bayar:bayar})
  	}
  	return result
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]