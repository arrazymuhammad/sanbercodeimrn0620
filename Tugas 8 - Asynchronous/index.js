// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Tulis code untuk memanggil function readBooks di sini
function call(time, books,i) {
	readBooks(time, books[i], function(t){
		if(t > 0 && i < books.length-1) call(t, books, i+1)
	})
}

call(10000, books, 0)
