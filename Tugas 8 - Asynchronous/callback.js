// di callback.js
function readBooks(time, book, callback ) {
    console.log(`saya membaca ${book.name}`)
    setTimeout(function(){
        let sisaWaktu = 0
        sisaWaktu = time - book.timeSpent
        if(time > book.timeSpent) {
            console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
            callback(sisaWaktu) //menjalankan function callback
        } else {
            console.log('waktu saya habis')
            callback(sisaWaktu)
        }   
    }, book.timeSpent)
}
 
module.exports = readBooks 