// Soal 1

let i = 2
console.log("LOOPING PERTAMA")
while(i <= 20){
	console.log(`${i} - I love coding`)
	i++
}
console.log("LOOPING KEDUA")
while(i >= 2){
	console.log(`${i} - I will become a mobile developer`)
	i--
}

// Soal 2
console.log("OUTPUT")
for (let i = 1; i <= 20; i++) {
	mod = i%2
	mod3 = (i%3 == 0) ? true : false
	if(mod == 0) console.log(`${i} - Berkualitas`)
	if(mod == 1 && !mod3) console.log(`${i} - Santai`)
	if(mod == 1 && mod3) console.log(`${i} - I Love Coding `)
}

// Soal 3
for(let i = 0; i < 4; i++){
	output = ""
	for(let j = 0; j < 8; j++){
		output+="#"
	}
	console.log(output)
}

// Soal 4
for(let i = 1; i <= 7; i++){
	output = ""
	for(let j = 0; j < i; j++){
		output+="#"
	}
	console.log(output)
}

// Soal 4
for(let i = 0; i < 8; i++){
	output = ""
	for(let j = 0; j < 4; j++){
		output+= (i%2 == 0) ? " #" : "# "
	}
	console.log(output)
}