class Animal {
	legs = 4
	cold_blooded = false
    constructor(name){
    	this.name = name
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


class Ape extends Animal{
	constructor(name){
		super(name)
		this.legs = 2
	}
	yell(){
		console.log("Auooo")
	}
}

class Frog extends Animal{
	constructor(name){
		super(name)
	}
	jump(){
		console.log("hop hop")
	}
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 