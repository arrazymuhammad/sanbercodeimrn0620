// Soal no 1
console.log("\n\nSoal No 1\n")

const range = function(start, end){
	if(!start || !end) return -1;
	let new_array = [];
	if (start < end){
		for (var i = start; i <= end; i++) {
			new_array.push(i)
		}
	}else{
		for (var i = start; i >= end; i--) {
			new_array.push(i)
		}
	}
	return new_array
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal no 2
console.log("\n\nSoal No 2\n")

const rangeWithStep = function(start, end, step){
	if(!start || !end) return -1;
	let new_array = [];
	if (start < end){
		for (var i = start; i <= end; i=i+step) {
			new_array.push(i)
		}
	}else{
		for (var i = start; i >= end; i=i-step) {
			new_array.push(i)
		}
	}
	return new_array
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal no 3
console.log("\n\nSoal No 3\n")

const sum = function(start = 0, end = 0, step = 1){
	let new_sum = 0;
	if (start < end){
		for (var i = start; i <= end; i=i+step) {
			new_sum +=i
		}
	}else{
		for (var i = start; i >= end; i=i-step) {
			new_sum += i
		}
	}
	return new_sum
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal no 4
console.log("\n\nSoal No 4\n")

const dataHandling = function(input){
	let new_string = "";
	for(item of input){
		new_string += `
No ID: ${item[0]}
Nama Lengkap: ${item[1]}
TTL:  ${item[2]} ${item[3]}
Hobi:  ${item[4]}
`	
	}
	return new_string;
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 
console.log(dataHandling(input))


// Soal no 5
console.log("\n\nSoal No 5\n")

const balikKata = function(word) {
	new_word = ""
	for (var i = word.length - 1; i >= 0; i--) {
		new_word += word[i]
	}
	return new_word
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal no 6
console.log("\n\nSoal No 6\n")

const dataHandling2 = function(input){
	console.log('keluaran yang diharapkan (pada console)\n')

	input.splice(1,1,input[1]+" Elsharawy")
	input.splice(2,1,"Provinsi "+input[2])
	input.splice(4,0,"Pria")
	input.splice(5,1,"SMA Internasional Metro")
	console.log(input)

	tanggal_split = input[3].split("/")
	console.log(namaBulan(tanggal_split[1]))

	tanggal_sort = tanggal_split.sort((a,b) => b-a)
	console.log(tanggal_sort)
	
	tanggal_join = tanggal_split.join("-")
	console.log(tanggal_join)
	/** 
	* 	Format akan mengikuti hasil sort karena fungsi sort mengubah data array asli
	*	Apabila masih ingin bertahan dengan format D-M-Y maka harus mengambil dari data source input[3]
	**/

	nama_slice = input[1].slice(0,15)
	console.log(nama_slice)

}

const namaBulan = function(bulan){
	let list_bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", 
					"Agustus", "September", "Oktober", "November", "Desember"]
	return list_bulan[bulan-1]
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */  