import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';

import Note from './Note';

export default class Main extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      noteArray: [],
      noteText:''
    }
  }
  render(){
    const {noteArray, noteText} = this.state
    let notes = noteArray.map((val,key) => {
      return <Note key={key} keyval={key} val={val} deleteMethod={ () => this.deleteNote(key) }/>
    })
    
    return(
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>
            - NOTER -
          </Text>
        </View>
        <ScrollView style={styles.scrollContainer}>
          {notes}
        </ScrollView>
        <View style={styles.footer}>
          <TextInput style={styles.textInput}
            placeholder=""
            placeholderTextColor="white"
            underlineColorAndroid="none"
            onChangeText={(noteText) => this.setState({noteText})}
            value={noteText}
          />
        </View>
        <TouchableOpacity style={styles.addButton} onPress={this.addNote.bind(this)}>
          <Text style={styles.addButtonText}>+</Text>
        </TouchableOpacity>
      </View>
    )
  }

  addNote(){
    const {noteArray, noteText} = this.state
    if(noteText.length > 0){
      noteArray.push({
        date : this.getDate(),
        note : noteText
      })
      this.setState({
        noteArray,
        noteText:""
      })
      console.log(noteArray)
    }
  }

  deleteNote(key){
    const {noteArray} = this.state
    noteArray.splice(key,1)
    this.setState({noteArray})
  }

  getDate(){
    var d = new Date()
    return `${d.getFullYear()} / ${d.getMonth()+1} / ${d.getDate()}`
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:36
  },
  header: {
    backgroundColor: '#E91E63',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 10,
    borderBottomColor: '#ddd',
  },
  headerText: {
    color: 'white',
    fontSize: 18,
    padding: 26,
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 10,
  },
  textInput: {
    alignSelf: 'stretch',
    color: '#fff',
    padding: 20,
    backgroundColor: '#252525',
    borderTopWidth: 2,
    borderTopColor: '#ededed',
  },
  addButton: {
    position: 'absolute',
    zIndex: 11,
    right: 20,
    bottom: 90,
    backgroundColor: '#E91E63',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24,
  },
});

