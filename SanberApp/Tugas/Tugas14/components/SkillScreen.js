import React, { Component } from 'react'
import { 
    StyleSheet,
    View,
    Image,
    Text,
    FlatList
} from 'react-native'
import logo from "../images/logo.png"
import {Ionicons} from '@expo/vector-icons'; 
import SkillItem from './SkillItem'
import skill_list from '../skillData.json'

export default class SkillScreen extends Component {
    constructor(props){
        super(props)
    }
    render(){
        const skill = skill_list.items[0]
        return (
                <View style={styles.container}>
                    <Image source={logo} style={styles.imgLogo} resizeMode="contain"/>
                    <View style={styles.profileContainer}>
                        <Ionicons name="md-person" size={40} color={darkBlue} />
                        <View style={styles.profileNameContainer}>
                            <Text style={{fontSize:12}}>Hai,</Text>
                            <Text style={{color:darkBlue, fontSize: 20, fontWeight:'bold'}}>Ar-Razy Muhammad</Text>
                        </View>
                    </View>
                    <Text style={{fontSize:40, paddingTop:10, color:darkBlue}}>SKILL</Text>
                    <View style={{backgroundColor:darkBlue, width:"100%", padding : 3, marginBottom:10}}></View>
                    <FlatList data={skill_list.items}
                        renderItem={(skill) => <SkillItem skill={skill}/>}
                        keyExtractor={(item) => item.id}
                        ItemSeparatorComponent={() => <View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
                        />
                </View>
        );
    }
}

const lightBlue = '#3EC6FF'
const darkBlue= '#003366'
const grayBackground = '#EFEFEF'
const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    paddingHorizontal: 20,
    flex: 1,
    backgroundColor: 'white'
  },
  imgLogo : {
      width : "50%",
      alignSelf : 'flex-end'
  },
  profileContainer :{
      flexDirection: "row",
  },
  profileNameContainer : {
      paddingHorizontal : 10,
  }

  
});
