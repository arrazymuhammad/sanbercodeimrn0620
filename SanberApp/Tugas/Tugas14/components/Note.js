import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { AntDesign } from '@expo/vector-icons'; 
export default class Note extends React.Component {
  render(){
    const {val, keyval,deleteMethod} = this.props
    return (
      <View key={keyval} style={styles.note}>
        <Text style={styles.noteText}>{val.date}</Text>
        <Text style={styles.noteText}>{val.note}</Text>

        <TouchableOpacity style={styles.noteDelete} onPress={deleteMethod}>
        <AntDesign name="delete" size={20} color="black" style={styles.noteDeleteText}/>
        </TouchableOpacity>

      </View>
    )
  }
};

const styles = StyleSheet.create({
  note: {
    position: 'relative',
    padding: 20,
    paddingRight: 100,
    borderBottomWidth: 2,
    borderBottomColor: '#ededed',
  },
  noteText: {
    paddingLeft: 20,
    borderLeftWidth: 10,
    borderLeftColor: '#e91e63',
  },
  noteDelete: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    borderRadius:5,
    padding: 10,
    top: 10,
    bottom: 10,
    right: 10
  },
  noteDeleteText: {
    color: 'white',
  }
});

