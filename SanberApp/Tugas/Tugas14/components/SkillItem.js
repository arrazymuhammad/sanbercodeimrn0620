import React, { Component } from 'react'
import { 
    Text, 
    View,
    StyleSheet
} from 'react-native'
import {MaterialCommunityIcons,EvilIcons} from '@expo/vector-icons'; 
export default class SkillItem extends Component {
    render(){
        const skill = this.props.skill.item
        console.log(skill)
        return(
            <View style={styles.container}>
                <View style={styles.iconContainer}>
                <MaterialCommunityIcons size={100} name={skill.iconName} color={darkBlue} style={styles.icon}/>
                </View>
                
                <View style={styles.skillDetail}>
                    <Text style={styles.skillName}>{skill.skillName}</Text>
                    <Text style={styles.categoryName}>{skill.categoryName}</Text>
                    <Text style={styles.percentageProgress}>{skill.percentageProgress}</Text>
                </View>
                <View style={styles.iconContainer}>
                <EvilIcons name="chevron-right" size={100} color={darkBlue}  style={styles.icon} />
                </View>
                
            </View>
        )
    }
}


const lightBlue = '#3EC6FF'
const darkBlue= '#003366'
const grayBackground = '#EFEFEF'
const styles = StyleSheet.create({
  container: {
    marginVertical: 20,
    borderRadius:10,
    padding: 15,
    flex: 1,
    backgroundColor: "#0fF",
    flexDirection : 'row'
  },
  iconContainer : {
    justifyContent : 'center'
  },
  skillDetail : {
    paddingLeft :30,
    width:"60%",
  },    
  skillName:{
    fontSize:35,
    fontWeight:"bold"
  },
  categoryName : {
      color:"#04f",
      fontSize:20,
      fontWeight:"bold"
  },
  percentageProgress : {
    alignSelf:"flex-end",
    color:"white",
    fontSize:55,
    fontWeight:"bold"
  }

});
