import React from 'react';
import { 
  Platform, StyleSheet, Text, View,
  Image, TouchableOpacity, TouchableHighlight, FlatList, TextInput, Button 
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import logo from "./images/logo.png"
import { Ionicons } from '@expo/vector-icons';

export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    const navigation = this.props.navigation
    this.state = {
      regis : true,
      username:'',
      email:'',
      pass:'',
      c_pass:'',
      navigation
    };
    this.handleRegisLogin = this.handleRegisLogin.bind(this)
    this.handleNav = this.handleNav.bind(this)
  }
  handleNav = () => {
    this.state.navigation.navigate('Profile')
  }
  handleRegisLogin = () => {
    this.setState({regis : !this.state.regis})
  }
  render(){
    const {regis,username,email,pass,c_pass} = this.state
	  return (
	  	<View style={styles.container}>
	  		<Image source={logo} style={styles.imgLogo}/>
        <Text style={styles.loginTitle}> Register </Text>
        <View style={styles.inputGroup}>
          <Text style={styles.inputLabel}>Username</Text>
          <TextInput style={styles.textInput} value={this.state.username}/>
        </View>
        { regis &&
          <View style={styles.inputGroup}>
            <Text style={styles.inputLabel}>Email</Text>
            <TextInput style={styles.textInput} value={this.state.email}/>
          </View>
        }
        <View style={styles.inputGroup}>
          <Text style={styles.inputLabel}>Password</Text>
          <TextInput secureTextEntry style={styles.textInput} value={this.state.pass}/>
        </View>
        { regis && 
          <View style={styles.inputGroup}>
            <Text style={styles.inputLabel}>Ulangi Password</Text>
            <TextInput secureTextEntry style={styles.textInput} value={this.state.c_pass}/>
          </View>
        }
        {
          regis
          ?
          <View style={styles.buttonGroup}>
            <TouchableHighlight style={styles.daftarButton} onPress={this.handleNav}>
              <Text style={styles.buttonText}>Daftar</Text>
            </TouchableHighlight>
            <Text style={{alignSelf: 'center' , padding: 20, fontSize: 20, color: lightBlue}}>atau</Text>
            <TouchableHighlight style={styles.masukButton} onPress={this.handleRegisLogin}>
              <Text style={styles.buttonText}>Masuk</Text>
            </TouchableHighlight>
          </View>
          :
          <View style={styles.buttonGroup}>
            <TouchableHighlight style={styles.masukButton} onPress={this.handleNav}>
              <Text style={styles.buttonText}>Masuk</Text>
            </TouchableHighlight>
            <Text style={{alignSelf: 'center' , padding: 20, fontSize: 20, color: lightBlue}}>atau</Text>
            <TouchableHighlight style={styles.daftarButton} onPress={this.handleRegisLogin}>
              <Text style={styles.buttonText}>Daftar</Text>
            </TouchableHighlight>
          </View>
        }
	  	</View>
	  );
  }
}
const lightBlue = '#3EC6FF'
const darkBlue= '#003366'
const grayBackground = '#EFEFEF'
const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    flex: 1,
  },
  imgLogo:{
    width: "100%",
    marginTop: 100
  },
  loginTitle:{
    color: darkBlue,
    fontSize: 26,
    textAlign: 'center',
    paddingTop: 50
  },
  inputGroup:{
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  inputLabel:{
    fontSize: 14,
    color: darkBlue
  },
  textInput:{
    fontSize: 18,
    paddingHorizontal: 10,
    width: '100%',
    borderColor: darkBlue,
    borderWidth: 1
  }, 
  buttonContainer:{
    width: '40%',
    alignSelf: 'center' 
  },
  buttonGroup:{
    paddingTop: 20
  },
  daftarButton : {
    backgroundColor: darkBlue,
    paddingVertical: 10,
    borderRadius: 20,
    width: '40%',
    alignSelf: 'center' 
  },
  masukButton : {
    backgroundColor: lightBlue,
    paddingVertical: 10,
    borderRadius: 20,
    width: '40%',
    alignSelf: 'center' 
  },
  buttonText:{
    color: 'white',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold'   
  }

});
