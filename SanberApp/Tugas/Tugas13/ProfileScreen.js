import React from 'react';
import { 
  Platform, StyleSheet, Text, View,Linking ,
  Image, TouchableOpacity, TouchableHighlight, FlatList, TextInput, Button 
} from 'react-native';

import logo from "./images/logo.png"
import { Ionicons } from '@expo/vector-icons';
import { Fontisto } from '@expo/vector-icons'; 
import { NavigationContainer } from '@react-navigation/native';

export default class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };
   
  }
  render(){
	  return (
	  	<View style={styles.container}>
	  	  <Text style={styles.title}>Tentang Saya</Text>
        <Image style={styles.imageProfile} source={{ uri: 'https://randomuser.me/api/portraits/men/32.jpg' }}/>
        <Text style={styles.name}>Ar-Razy Muhammad</Text>
        <Text style={styles.tagline}>Web Developer</Text>
        <View style={styles.detailContainer}>
          <Text style={styles.detailTitile}>Portofolio</Text>
          <View style={styles.garis}/>
          <View style={styles.portofolioContainer}>
            <TouchableOpacity style={styles.portofolioItem} onPress={ () => Linking.openURL('https://github.com/arrazymuhammad') }>
              <Ionicons name="logo-github" size={45} color={lightBlue} />
              <Text style={styles.portousername}>@arrazymuhammad</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.portofolioItem} onPress={ () => Linking.openURL('https://gitlab.com/arrazymuhammad') }>
              <Fontisto name="gitlab" size={45} color={lightBlue} />
              <Text style={styles.portousername}>@arrazymuhammad</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.detailContainer}>
          <Text style={styles.detailTitile}>Hubungi Saya</Text>
          <View style={styles.garis}/>
           <View style={styles.contactContainer}>
            <TouchableOpacity style={styles.contactItem} onPress={ () => Linking.openURL('https://fb.me/arrazymuhammad') }>
              <Ionicons name="logo-facebook" size={45} color={darkBlue} />
              <Text style={styles.contactUsername}>@arrazymuhammad</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.contactItem}  onPress={ () => Linking.openURL('https://instagram.com/arrazymuhammad') }>
              <Ionicons name="logo-instagram" size={45} color={darkBlue} />
              <Text style={styles.contactUsername}>@arrazymuhammad</Text>
            </TouchableOpacity>
          </View>
        </View>
	  	</View>
	  );
  }
}
const lightBlue = '#3EC6FF'
const darkBlue= '#003366'
const grayBackground = '#EFEFEF'
const styles = StyleSheet.create({
  container: {
    paddingTop: 80,
    paddingHorizontal: 40,
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white'
  },
  title:{
    fontSize: 36,
    color: darkBlue,
    fontWeight: 'bold',
    paddingVertical: 20
  },
  imageProfile:{
    width: 250,
    height: 250,
    borderRadius: 250,
    paddingVertical: 20
  },
  name:{
    color: darkBlue,
    fontSize: 32,
    paddingTop: 20,
    fontWeight: 'bold'
  },
  tagline:{
    color: lightBlue,
    fontSize: 16,
    fontWeight: 'bold'
  },
  detailContainer : {
    backgroundColor: grayBackground,
    padding: 20,
    marginVertical: 20,
    width: '100%'
  },
  detailTitile : {
    fontWeight: 'bold',
    fontSize: 16,
    color: darkBlue,
    alignSelf: 'flex-start'
  },
  garis :{
    backgroundColor: darkBlue,
    width: '100%',
    paddingVertical: 2,
    marginVertical: 3
  },
  portousername : {
    color:darkBlue
  },
  portofolioItem :{
    alignItems: 'center',
    paddingTop : 25,
    paddingBottom: 10
  },
  portofolioContainer : {
    flexDirection: 'row',
    justifyContent: 'space-around' 
  },
  contactContainer : {

  },
  contactItem : {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  contactUsername : {
    paddingHorizontal: 10,
    paddingVertical: 12,
    fontSize: 16,
    color: darkBlue
  },  

  
});
